package com.example.gracjan.aa

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import cloud.proxi.sdk.ProxiCloudServiceMessage
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by gracj on 2017-12-25.
 */


class TestActivity : AppCompatActivity() {
    private val MY_PERMISSION_REQUEST_LOCATION_SERVICES = 0x200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder(this).apply {
                    setTitle("Functionality limited")
                    setMessage("Since location access has not been granted, " + "this app will not be able to discover beacons when in the background.")
                    setPositiveButton(android.R.string.ok, null)
                    setOnDismissListener({
                        ActivityCompat.requestPermissions(
                                this@TestActivity,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                MY_PERMISSION_REQUEST_LOCATION_SERVICES)
                    })
                    show()
                }
            } else {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSION_REQUEST_LOCATION_SERVICES)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSION_REQUEST_LOCATION_SERVICES -> {
                val sdk = DemoApplication.instance.sdk
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sdk.sendLocationFlagToReceiver(ProxiCloudServiceMessage.MSG_LOCATION_SET)
                } else {
                    sdk.sendLocationFlagToReceiver(ProxiCloudServiceMessage.MSG_LOCATION_NOT_SET_WHEN_NEEDED)
                }
                return
            }
        }
    }
}
