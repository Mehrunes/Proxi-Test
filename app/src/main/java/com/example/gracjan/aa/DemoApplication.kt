package com.example.gracjan.aa

import android.app.Application
import cloud.proxi.BackgroundDetector
import cloud.proxi.ProxiCloudSdk
import cloud.proxi.sdk.Logger


/**
 * Created by gracj on 2017-12-22.
 */
class DemoApplication : Application() {
    internal val sdk: ProxiCloudSdk by lazy { ProxiCloudSdk(this, getString(R.string.proxi_cloud_api_key)) }
    private val detector: BackgroundDetector by lazy { BackgroundDetector(sdk) }

    companion object {
        lateinit var instance: DemoApplication
            private set

        init {
            Logger.enableVerboseLogging()
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this@DemoApplication
        registerActivityLifecycleCallbacks(detector)
    }
}