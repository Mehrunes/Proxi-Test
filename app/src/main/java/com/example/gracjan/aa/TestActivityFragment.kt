package com.example.gracjan.aa

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cloud.proxi.sdk.action.ActionType
import cloud.proxi.sdk.action.InAppAction
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_main.*

class TestActivityFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sdk = DemoApplication.instance.sdk
        sdk.registerEventListener { beaconEvent ->
            val action = beaconEvent.action
            when (action.type) {
                ActionType.MESSAGE_IN_APP -> {
                    val inApp = action as InAppAction
                    Toast.makeText(context, "Otrzymałeś Obrazek!", Toast.LENGTH_SHORT).show()
                    textView.text = inApp.subject
                    Picasso.with(context)
                            .load(inApp.uri)
                            .into(imageView)
                }
            }
        }
    }
}
