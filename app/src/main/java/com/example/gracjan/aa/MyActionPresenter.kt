package com.example.gracjan.aa

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.app.NotificationCompat
import cloud.proxi.sdk.action.*


/**
 * Created by gracj on 2017-12-22.
 */
class MyActionPresenter : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val action: Action = intent.extras.getParcelable(Action.INTENT_KEY)

        when (action.type) {
            ActionType.MESSAGE_URI -> {
                val uriMessageAction = action as UriMessageAction
                showNotification(context, action.getUuid().hashCode(), uriMessageAction.title, uriMessageAction.content, Uri.parse(uriMessageAction.uri))
            }
            ActionType.MESSAGE_WEBSITE -> {
                val visitWebsiteAction = action as VisitWebsiteAction
                showNotification(context, action.getUuid().hashCode(), visitWebsiteAction.subject, visitWebsiteAction.body, visitWebsiteAction.uri)
            }
            ActionType.MESSAGE_IN_APP -> {
                val inAppAction = action as InAppAction
                showNotification(context, action.getUuid().hashCode(), inAppAction.subject, inAppAction.body, inAppAction.uri)
            }
        }
    }

    private fun showNotification(context: Context, id: Int, title: String, content: String, uri: Uri) {
        val notification = NotificationCompat.Builder(context)
                .setContentIntent(PendingIntent.getActivity(
                        context,
                        0,
                        Intent(Intent.ACTION_VIEW, uri),
                        PendingIntent.FLAG_UPDATE_CURRENT))
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setShowWhen(true)
                .build()
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(id, notification)
    }
}